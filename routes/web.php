<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/resetPasswordLogged', 'Admin\UsersController@Reset')->name('resetPasswordLogged');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'admin'], function () {
    // Route::resource('/roles', 'Admin\RolesController');
    Route::resource('/users', 'Admin\UsersController');
    // Route::resource('admin/permission', 'Admin\PermissionsController');
});

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin', 'gerente']], function () {
    Route::resource('/clientes', 'ClienteController');
    Route::resource('/fornecedores', 'FornecedorController');
    Route::resource('/categorias', 'CategoriaController');
});

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin', 'gerente', 'funcionario']], function () {
    Route::resource('/produtos', 'ProdutoController');
    Route::get('movimentos/entrada', 'MovimentoController@entrada')->name('entrada');
    Route::get('movimentos/saida', 'MovimentoController@saida')->name('saida');
    Route::get('movimentos/print/{id}', 'MovimentoController@generatePDF')->name('print');
    Route::resource('movimentos', 'MovimentoController');
    Route::get('/relatorios/movimentoperiodo', 'MovimentoPeriodoController@index')->name('movimentoperiodo');
    Route::get('/relatorios/posicaoestoque_generate-pdf','PosicaoDeEstoqueController@generatePDF')->name('PDFposicaoestoque');
    Route::get('/relatorios/movimentoperiodo_generate-pdf','MovimentoPeriodoController@generatePDF')->name('PDFmovimentoperiodo');
    Route::get('/relatorios/movimentocliente_generate-pdf/{id}','MovimentoClienteController@generatePDF')->name('PDFmovimentocliente');
    Route::get('/relatorios/movimentofornecedor_generate-pdf/{id}','MovimentoFornecedorController@generatePDF')->name('PDFmovimentofornecedor');
    Route::get('/relatorios/movimentoproduto_generate-pdf/{id}','MovimentoProdutoController@generatePDF')->name('PDFmovimentoproduto');
    Route::get('/relatorios/movimentocategoria_generate-pdf/{id}','MovimentoCategoriaController@generatePDF')->name('PDFmovimentocategoria');
    Route::get('/relatorios/movimentousuario_generate-pdf/{id}','MovimentoUsuarioController@generatePDF')->name('PDFmovimentousuario');
});
