function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('logradouro').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('estado').value=("");
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('logradouro').value=(conteudo.logradouro);
        document.getElementById('bairro').value=(conteudo.bairro);
        document.getElementById('cidade').value=(conteudo.localidade);
        document.getElementById('estado').value=(conteudo.uf);
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}

function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('logradouro').value="...";
            document.getElementById('bairro').value="...";
            document.getElementById('cidade').value="...";
            document.getElementById('estado').value="...";

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};


$(document).ready(function(){
    var MascaraTelefone = function (val)
    {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    telefoneOpcoes =
    {
        onKeyPress: function(val, e, field, options)
        {
            field.mask(MascaraTelefone.apply({}, arguments), options);
        }
    };
    $('.telefone').mask(MascaraTelefone, telefoneOpcoes);
    $('.cep').mask('00000-000');
});

function pageBack()
{
    window.history.go(-1);
};

function pageFoward()
{
    window.history.go(+1);
};

function submitFormCliente()
{
    $("#form-cliente").attr('action', "movimentocliente_generate-pdf");
    $("#form-cliente").submit();
};

function submitFormFornecedor()
{
    $("#form-fornecedor").attr('action', "movimentofornecedor_generate-pdf");
    $("#form-fornecedor").submit();
};

function submitFormProduto()
{
    $("#form-produto").attr('action', "movimentoproduto_generate-pdf");
    $("#form-produto").submit();
};

function submitForm()
{
    $("#form-filtro").attr('action', "movimentoperiodo_generate-pdf");
    $("#form-filtro").submit();
};

$(function ()
{
    $('#date1').datetimepicker({
        locale: 'br',
        format: 'DD/MM/YYYY HH:mm:ss',
    });
    $('#date2').datetimepicker({
        locale: 'br',
        format: 'DD/MM/YYYY HH:mm:ss',
        useCurrent: false //Important! See issue #1075
    });
    $("#date1").on("dp.change", function (e) {
        $('#date2').data("DateTimePicker").minDate(e.date);
    });
    $("#date2").on("dp.change", function (e) {
        $('#date1').data("DateTimePicker").maxDate(e.date);
    });
});
