# Gestão de Estoque DataFibra  #

Este é o projeto de Trabalho de Conclusão de Curso feito pelo aluno Renan de Oliveira, concluinte do curso de Ciência da Computação da Universidade Estadual do Centro-Oeste (Unicentro)

### Resumo ###

O sistema é uma solução automatizada para controle de estoque de materiais de uma empresa de telecomunicações localizada em
Guarapuava, Paraná. Uma das principais motivações para a criação deste sistema é permitir a centralização dos dados, de 
forma que pessoas em diferentes locais consigam ter acesso simultâneo às informações de estoque da empresa, diminuindo assim
os problemas e o trabalho repetitivo causados pela ineficiência das planilhas utilizadas até então.

### Tecnologias empregadas no projeto ###

* PHP 7 ou Superior
* Composer
* MySQL MariaDB
* Framework Laravel 5.6
* Git 2.16

### Configuração para a importação do projeto ###

* Crie ou escolha um diretório qualquer para o projeto
* Acesse este diretório por linha de comandos
* Execute os seguintes comandos:
    * git clone git@bitbucket.org:ranemj77/softwaretcc.git para o acesso via SSH ou git clone https://ranemj77@bitbucket.org/ranemj77/softwaretcc.git para o acesso via HTTPS
    * git fetch && git checkout master
    * composer update (para as dependencias)
* Em seguida, é necessário configurar o .env para indicar a base de dados, basta duplicar o arquvio .env.example e renomear esta cópia para .env e então informar os dados de acesso ao banco de dados DB_DATABASE deve conter o nome do database que será utilizado (caso não exista, deve ser criado utilizando a interface do SGBD), DB_USERNAME deve conter o usuário que acessará o banco de dados e DB_PASSWORD a senha de acesso deste usuário.
* Realizada esta etapa, é necessário criar a estrutura do banco e alimentá-la:
    * php artisan migrate:install (irá criar a tabela de migrations)
    * php artisan migrate:fresh (irá excluir as tabelas existentes e criar as tabelas necessárias, de acordo com a especificação do projeto)
        * php artisan db:seed (irá alimentar as tabelas com dados iniciais e para testes)
        * php artisan serve (irá iniciar um servidor local e informará o endereço e porta que estará disponível para acessar o sistema)
        
* Acessar o sistema no endereço fornecido pelo passo anterior e utilizar os seguintes dados de acesso, pois é um usuário com privilégio de administrador do sistema e poderá executar quaisquer atividades:
    * login: admin@admin.com
    * senha: 123456

### Nota do autor ###

* Este é um sistema feito para uma solução empresarial, como Trabalho de Conclusão de Curso
* Portanto possui toda a autoria protegida por assinaturas, sob a tutela do Departamento de Ciência da Computação (DECOMP) da Universidade Estadual do Centro-Oeste (Unicentro)
* Contém na proposta as assinaturas:
    * Do aluno autor
    * Da empresa
    * Do orientador responsável