@extends('adminlte::page')

@section('scripts')

@stop

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerMovimentosRelatorios')

<style>
@media only screen and (min-width: 950px)
{
    .messageHeader:after
    {
        content: "Movimentos (Relatório Especial)";
    }
    .messageSubHeader:after
    {
        content: "Movimentação por periodo";
    }
}
@media only screen and (max-width: 949px)
{
    .box-header>.box-tools
    {
        position: static !important;
        margin-top: 10px;
    }
    .textSearch
    {
        width: 100%;
    }
    .messageHeader:after
    {
        content: "Movimentos";
    }
    .messageSubHeader:after
    {
        content: "Movimentação por periodo";
    }
}

@media only screen and (max-width: 795px)
{
    .input-margin
    {
        margin-bottom: 5px;
    }
}

@media only screen and (min-width: 600px)
{
    .box-header>.box-tools
    {
        right: 12px !important;
        top: 9px !important;
    }
}

@media only screen and (min-width: 570px)
{
    .dateInput
    {
        width: 11.2em !important;
    }
}
@media only screen and (max-width: 569px)
{

    .message:after
    {
        content: "Gerar Relatório";
    }
    .dateInput
    {
        width: 8.2em !important;
    }
}
</style>

@include('templates.alerts')

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">
            <a href="#" class="btn btn-success btn-ms" title="Emitir Relatorio" onclick="submitForm();">
                <i class="fa fa-download" aria-hidden="true"></i> <span>Emitir Relatório</span>
            </a>
        </h3>
        <div class="box-tools">
            <form method="GET" action="{{ url('relatorios/movimentoperiodo') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search" id="form-filtro">
                <div class='input-group date input-margin' id='date1'>
                    <input type='text' class="form-control textSearch" name='date1' value="{{ request('date1') }}" placeholder="De..."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class='input-group date input-margin' id='date2'>
                    <input type='text' class="form-control textSearch" name='date2' value="{{ request('date2') }}" placeholder="Até..."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <button class="btn btn-secondary" type="submit">
                    <i class="fa fa-search"></i> Buscar
                </button>
            </form>
        </div>
    </div>
@include('templates.tableMovimentos')

</div>

@endsection
