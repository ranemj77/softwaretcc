<html>

<head>

	<title>Impressão de Movimento</title>

<style>
  	footer
	{
		position: fixed;
		bottom: -40px;
		left: 0px;
		right: 0px;
		height: 50px;
	}
	#show
	{
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: 12px;
	    border-collapse: collapse;
	    width: 100%;
	}

	#show td, #show th
	{
	    border: 1px solid #ddd;
	    padding: 8px;
	}

	#show tr:nth-child(even)
	{
		background-color: #f2f2f2;
	}

	#show tr:hover
	{
		background-color: #ddd;
	}

	#show th
	{
	    padding-top: 12px;
	    padding-bottom: 12px;
	    text-align: center;
	    background-color: #6666ff;
	    color: white;
	}
	.header
	{
		font-family: Arial, Helvetica, sans-serif;
	}
</style>

</head>

<body>
<div class="header">
    <span style="float:right">
		<img src = "{{$img_path}}" style="width: 12em; height: 3em">
	</span>
    <h2 align="center">
		{{ $title }}
	</h2>
</div>
<hr>

<table id="show">
	<tbody>
		<tr>
			<th>Usuário Atual: </th>
			<td>{{$current_user}}</td>
			<th>Funcionário Cadastrante do Movimento: </th>
			<td>{{$cadastrante}}</td>
			<th>Gerado em: </th>
			<td>{{$footerDate}}</td>
		</tr>
	</tbody>
</table>

<hr>

<table id="show">
    <tbody>
    <tr>
        <th>Código</th>
        <td width="100">{{ $movimento->codigo }}</td>
        <th>Fornecedor</th>
        <td width="100">{{ $movimento->fornecedor_nome }}</td>
        <th>Cliente</th>
        <td width="100">{{ $movimento->cliente_nome }} </td>
    </tr>
    <tr>
        <th> Produto</th>
        <td width="100"> {{ $movimento->produto_nome }} </td>
        <th> Tipo </th>
        <td width="100"> {{ $movimento->tipo }} </td>
        <th> Motivo </th>
        <td width="100"> {{ $movimento->motivo }} </td>
    </tr>
    <tr>
        <th> Quantidade </th>
        <td width="100"> {{ $movimento->quantidade }} </td>
        <th> Valor Unitario </th>
        <td width="100"> R$ {{ number_format($movimento->valor_unit, 2)}} </td>
        <th> Valor Total </th>
        <td width="100"> R$ {{ number_format($movimento->valor_total, 2)}} </td>
    </tr>
	<tr>
        <th> Descricao </th>
        <td colspan="5"> {{ $movimento->descricao }} </td>
    </tr>
    </tbody>
</table>
<br>
<br>
<br>
<br>
<br>
<h3 class="header">Assinatura do Responsável</h3>
<br>
<p>_____________________________________________</p>

<footer>
  <hr>
  <div>
    <span style="float:right">{{$version}}</span>
    <span style="float:left">DataFibraEstoqueFácil</span>
  </div>
</footer>
</body>

</html>
