<html>

<head>

	<title>Relatorio Posição de Estoque</title>

  <style>
      footer
	  {
		  position: fixed; bottom: 0px; left: 0px; right: 0px; height: 50px;
	  }
      p
	  {
		  page-break-after: always;
	  }
      p:last-child
	  {
		  page-break-after: never;
	  }
	  .header
	  {
		  font-family: Arial, Helvetica, sans-serif;
	  }
	  #header
	  {
	      font-family: Arial, Helvetica, sans-serif;
	      font-size: 12px;
	      border-collapse: collapse;
	      width: 100%;
	  }

	  #header td, #header th
	  {
	      border: 1px solid #ddd;
	      padding: 8px;
	  }

	  #header tr:nth-child(even)
	  {
		  background-color: #f2f2f2;
	  }

	  #header tr:hover
	  {
		  background-color: #ddd;
	  }

	  #header th {
	      padding-top: 12px;
	      padding-bottom: 12px;
	      text-align: center;
	      background-color: #6666ff;
	      color: white;
	  }
  </style>
</head>

<body>
<div class="header">
    <span style="float:right"><img src = "{{$img_path}}" style="width: 12em; height: 3em"></span>
    <h2 align="center">{{ $title }}</h2>
</div>
<hr>

<table id="header">
	<tbody>
		<tr>
			<th>Usuário Atual: </th>
			<td>{{$current_user}}</td>
			<th>Gerado em: </th>
			<td>{{$footerDate}}</td>
		</tr>
		<tr>
			<th>C. Médio Estoque: </th>
			<td>R$ {{number_format($custoMedioEstoque, 2)}}</td>
			<th>V. Total Estoque: </th>
			<td>R$ {{number_format($valorTotalEstoque, 2)}}</td>
		</tr>
	</tbody>
</table>
<hr>

<main>
@include('templates.tablePosicaoRelatorio')
<div>
	<hr>
	<span style="float:right">{{$version}}</span>
	<span>DataFibraEstoqueFácil</span>
</div>
</main>

@include('templates.pageFooter')

</body>

</html>
