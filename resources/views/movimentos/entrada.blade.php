@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerMovimentosEntradas')

@include('templates.alerts')

<style>

.messageHeader:after
{
    content: "Movimentos";
}
.messageSubHeader:after
{
    content: "Criar Nova Entrada de Estoque";
}

@media only screen and (min-width: 400px)
{
    .messageEntrada:after
    {
        content: "Entrada";
    }
    .messageSaida:after
    {
        content: "Saida";
    }
}
</style>
    <div class="box box-danger">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header"><h3>Criar uma nova Entrada</h3></div>
                        <div class="card-body">
                            <a href="{{ url('/movimentos') }}" title="Back">
                                <button class="btn btn-warning btn-ms">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar
                                </button>
                            </a>
                            <br/>
                            <br/>
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <form method="POST" action="{{ url('/movimentos') }}" accept-charset="UTF-8"
                                  class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                @include ('templates.selectFornecedor')
                                @include ('templates.selectProduto')
                                @include ('templates.MovimentoValorUnit')
                                @include ('templates.hiddenEntrada')
                                @include ('movimentos.form')

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
