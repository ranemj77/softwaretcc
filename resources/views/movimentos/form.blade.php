<div class="form-group {{ $errors->has('quantidade') ? 'has-error' : ''}}">
    <label for="quantidade" class="col-md-4 control-label">{{ 'Quantidade' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="quantidade" type="number" id="quantidade" min="1" value="{{ $movimento->quantidade or ''}}" >
        {!! $errors->first('quantidade', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('motivo') ? 'has-error' : ''}}">
    <label for="motivo" class="col-md-4 control-label">{{ 'Motivo' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="motivo" type="text" id="motivo" value="{{ $movimento->motivo or ''}}" >
        {!! $errors->first('motivo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-md-4 control-label">{{ 'Descricao' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" required>{{ $movimento->descricao or ''}}</textarea>
        {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Criar' }}">
    </div>
</div>
