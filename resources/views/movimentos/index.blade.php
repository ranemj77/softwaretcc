@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerMovimentos')

@include('templates.alerts')

<style>
.box-header>.box-tools
{
    right: 12px !important;
    top: 9px !important;
}
.btn-padding
{
    padding-left: 5px;
}
.messageHeader:after
{
    content: "Movimentos";
}
.messageSubHeader:after
{
    content: "Visualizar Movimentos de Estoque";
}

@media only screen and (max-width: 490px)
{
    .box-header>.box-tools
    {
        position: static !important;
        margin-top: 10px;
    }
    .textSearch
    {
        width: 100%;
    }
}
</style>

    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title">
                <a href="{{ route('entrada') }}" class="btn btn-success btn-ms"
                   title="Adicionar Entrada">
                    <i class="fa fa-sign-in" aria-hidden="true"></i> <span>Entrada</span>
                </a>

                <a href="{{ route('saida') }}" class="btn btn-success btn-ms"
                   title="Adicionar Saida">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> <span>Saida</span>
                </a>
              </h3>
            <div class="box-tools">
                <div class="input-group input-group-ms textSearch">
                    <form method="GET" action="{{ url('/movimentos') }}" accept-charset="UTF-8"
                          class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Procurar..." value="{{ request('search') }}">
                            <div class="input-group-btn btn-padding">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        @include('templates.tableMovimentos')
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@endsection
