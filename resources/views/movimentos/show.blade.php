@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerMovimentos')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Movimentos";
}
.messageSubHeader:after
{
    content: "Mostrar Movimento de Estoque";
}
@media only screen and (max-width: 445px)
{
    .btn
    {
        margin-bottom: 5px;
    }
}
</style>

    <div class="box box-danger">
        <div class="container">
            <div class="row">

                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h3>Detalhes do Movimento {{ $movimento->codigo }} (Criado em : {{ $movimento->created_at->format('d/m/Y H:i:s') }})</h3>
                        </div>
                        <div class="card-body">
                            <a href="#" title="Voltar">
                                <button class="btn btn-warning btn-ms" onclick="pageBack()">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    <span>Voltar</span>
                                </button>
                            </a>
                            <a href="{{ route('print', $movimento->id)}}" title="Emitir Relatorio">
                                <button class="btn btn-success btn-ms"><i class="fa fa-download" aria-hidden="true"></i>
                                    <span>Emitir Relatório</span>
                                </button>
                            </a>
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table">
                                    <tbody>
                                    <tr>
                                        <th>Código</th>
                                        <td>{{ $movimento->codigo }}</td>
                                    </tr>
                                    <tr>
                                        <th>Usuário Cadastrante:</th>
                                        <td>{{ $movimento->user_name}}</td>
                                    </tr>
                                    <tr>
                                        <th> Fornecedor</th>
                                        <td> {{ $movimento->fornecedor_nome }} </td>
                                    </tr>
                                    <tr>
                                        <th> Cliente</th>
                                        <td> {{ $movimento->cliente_nome }} </td>
                                    </tr>
                                    <tr>
                                        <th> Produto</th>
                                        <td> {{ $movimento->produto_nome }} </td>
                                    </tr>
                                    <tr>
                                        <th> Tipo </th>
                                        <td> {{ $movimento->tipo }} </td>
                                    </tr>
                                    <tr>
                                        <th> Motivo </th>
                                        <td> {{ $movimento->motivo }} </td>
                                    </tr>
                                    <tr>
                                        <th> Quantidade </th>
                                        <td> {{ $movimento->quantidade }} </td>
                                    </tr>
                                    <tr>
                                        <th> Valor Unitario </th>
                                        <td> R$ {{ number_format($movimento->valor_unit, 2)}} </td>
                                    </tr>
                                    <tr>
                                        <th> Valor Total </th>
                                        <td> R$ {{ number_format($movimento->valor_total, 2)}} </td>
                                    </tr>
                                    <tr>
                                        <th> Descricao </th>
                                        <td> {{ $movimento->descricao }} </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
