<div class="form-group {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-md-4 control-label">{{ 'Nome' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="nome" type="text" id="nome" value="{{ $produto->nome or ''}}" placeholder="Ex: Nome do produto" required>
        {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('marca') ? 'has-error' : ''}}">
    <label for="marca" class="col-md-4 control-label">{{ 'Marca' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="marca" type="text" id="marca" value="{{ $produto->marca or ''}}" placeholder="Ex: Marca" required>
        {!! $errors->first('marca', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@include('templates.selectCategoria')
<div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-md-4 control-label">{{ 'Descricao' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" placeholder="Digite aqui uma descrição do produto" required title="Campo Necessário">
          {{ $produto->descricao or ''}}
        </textarea>
        {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Criar' }}">
    </div>
</div>
