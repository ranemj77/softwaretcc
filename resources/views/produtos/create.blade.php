@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerProdutos')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Cadastros";
}
.messageSubHeader:after
{
    content: "Cadastrar Produto";
}
</style>

<div class="box box-warning">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Criar novo Produto</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/produtos') }}" title="Back"><button class="btn btn-warning btn-ms"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/produtos') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('produtos.form')

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
