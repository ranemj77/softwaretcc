<div class="form-group {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-md-4 control-label">{{ 'Nome' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="nome" type="text" id="nome" value="{{ $fornecedore->nome or ''}}" placeholder="Ex: Nome do fornecedor" required>
        {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cep') ? 'has-error' : ''}}" >
    <label for="cep" class="col-md-4 control-label">{{ 'Cep' }}</label>
    <div class="col-md-6">
        <input class="form-control cep" name="cep" type="text" id="cep" value="{{ $fornecedore->cep or ''}}" onblur="pesquisacep(this.value);" placeholder="Ex: 99999-999" pattern="\d{5}[\-]\d{3}" required title="Insira conforme o formato 99999-999">
        {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('logradouro') ? 'has-error' : ''}}">
    <label for="logradouro" class="col-md-4 control-label">{{ 'Logradouro' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="logradouro" type="text" id="logradouro" value="{{ $fornecedore->logradouro or ''}}" placeholder="Ex: Rua" required>
        {!! $errors->first('logradouro', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-md-4 control-label">{{ 'Numero' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="numero" type="text" id="numero" value="{{ $fornecedore->numero or ''}}" placeholder="Ex: 0000" required>
        {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('bairro') ? 'has-error' : ''}}">
    <label for="bairro" class="col-md-4 control-label">{{ 'Bairro' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="bairro" type="text" id="bairro" value="{{ $fornecedore->bairro or ''}}" placeholder="Ex: Bairro" required>
        {!! $errors->first('bairro', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('cidade') ? 'has-error' : ''}}">
    <label for="cidade" class="col-md-4 control-label">{{ 'Cidade' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="cidade" type="text" id="cidade" value="{{ $fornecedore->cidade or ''}}" placeholder="Ex: Cidade" required>
        {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('estado') ? 'has-error' : ''}}">
    <label for="estado" class="col-md-4 control-label">{{ 'Estado' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="estado" type="text" id="estado" value="{{ $fornecedore->estado or ''}}" placeholder="Ex: UF" required>
        {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telefone1') ? 'has-error' : ''}}">
    <label for="telefone1" class="col-md-4 control-label">{{ 'Telefone Principal' }}</label>
    <div class="col-md-6">
        <input class="form-control telefone" name="telefone1" type="text" id="telefone1" value="{{ $fornecedore->telefone1 or ''}}" value="{{ $cliente->telefone1 or ''}}" placeholder="Ex: (99) 9999-9999" pattern="[\(]\d{2}[\)]\s(\d{4}|\d{5})[\-]\d{4}" required title="Insira conforme o formato (99) 9999-9999 ou (99) 99999-9999">
        {!! $errors->first('telefone1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telefone2') ? 'has-error' : ''}}">
    <label for="telefone2" class="col-md-4 control-label">{{ 'Outro Telefone' }}</label>
    <div class="col-md-6">
        <input class="form-control telefone" name="telefone2" type="text" id="telefone2" value="{{ $fornecedore->telefone2 or ''}}" value="{{ $cliente->telefone1 or ''}}" placeholder="Ex: (99) 9999-9999" pattern="[\(]\d{2}[\)]\s(\d{4}|\d{5})[\-]\d{4}" required title="Insira conforme o formato (99) 9999-9999 ou (99) 99999-9999">
        {!! $errors->first('telefone2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Criar' }}">
    </div>
</div>
