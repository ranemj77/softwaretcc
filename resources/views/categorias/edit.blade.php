@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerClientes')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Cadastros";
}
.messageSubHeader:after
{
    content: "Editar Categoria";
}
</style>

<div class="box box-warning">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Editar categoria {{ $categoria->nome }}</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/categorias') }}" title="Voltar"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/categorias/' . $categoria->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('categorias.form', ['submitButtonText' => 'Alterar'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
