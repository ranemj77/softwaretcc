@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerClientes')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Cadastros";
}
.messageSubHeader:after
{
    content: "Mostrar Categoria";
}
@media only screen and (max-width: 445px)
{
    .btn
    {
        margin-bottom: 5px;
    }
}
</style>

<div class="box box-warning">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Detalhes da Categoria {{ $categoria->nome }}</h3></div>
                    <div class="card-body">

                        <a href="#" title="Voltar"><button class="btn btn-warning btn-ms" onclick="pageBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <a href="{{ url('/categorias/' . $categoria->id . '/edit') }}" title="Edit categoria"><button class="btn btn-primary btn-ms"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>

                        <form method="POST" action="{{ url('categorias' . '/' . $categoria->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-ms" title="Delete categoria" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</button>
                        </form>
                        <a href="{{route('PDFmovimentocategoria', $categoria->id)}}" title="Emitir Relatorio">
                            <button class="btn btn-success btn-ms"><i class="fa fa-download" aria-hidden="true"></i>
                                <span>Emitir Relatório</span>
                            </button>
                        </a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table">
                                <tbody>
                                    <tr>
                                        <th>Código</th>
                                        <td>{{ $categoria->codigo }}</td>
                                    </tr>
                                    <tr>
                                        <th> Nome </th>
                                        <td> {{ $categoria->nome }} </td>
                                    </tr>
                                    <tr>
                                        <th> Descricao </th>
                                        <td> {{ $categoria->descricao }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <h3> Ultimos movimentos de produtos com esta categoria </h3>
                        @include('templates.tableMovimentos')

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
