<div class="form-group {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-md-4 control-label">{{ 'Nome' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="nome" type="text" id="nome" value="{{ $categoria->nome or ''}}" placeholder="Ex: Caixas" required title="Campo Necessário">
        {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-md-4 control-label">{{ 'Descricao' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" placeholder="Digite aqui uma descrição da categoria" required title="Campo Necessário">
            {{ $categoria->descricao or ''}}
        </textarea>
        {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Criar' }}">
    </div>
</div>
