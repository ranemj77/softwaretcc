@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerClientes')

@include('templates.alerts')

<style>
.box-header>.box-tools
{
    right: 12px !important;
    top: 9px !important;
}
.btn-padding
{
    padding-left: 5px;
}
.messageHeader:after
{
    content: "Cadastros";
}
.messageSubHeader:after
{
    content: "Visualizar Clientes";
}
@media only screen and (max-width: 490px)
{
    .box-header>.box-tools
    {
        position: static !important;
        margin-top: 10px;
    }
    .textSearch
    {
        width: 100%;
    }
}
</style>

    <div class="box box-warning">
        <div class="box-header">
            <a href="{{ url('/clientes/create') }}" class="btn btn-success btn-ms" title="Add New cliente">
                <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
            </a>
            <div class="box-tools">
                <div class="input-group input-group-sm textSearch">
                    <form method="GET" action="{{ url('/clientes') }}" accept-charset="UTF-8"
                          class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Procurar..." value="{{ request('search') }}">
                            <div class="input-group-btn btn-padding">
                                <button class="btn btn-" type="submit">
                                    <i class="fa fa-search"></i> Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Logradouro</th>
                        <th>Bairro</th>
                        <th>Cidade</th>
                        <th>Telefone</th>
                        <th>Visualizar</th>
                    </tr>
                    @foreach($clientes as $item)
                        <tr>
                            <td>{{ $item->codigo }}</td>
                            <td>{{ $item->nome }}</td>
                            <td>{{ $item->logradouro }}</td>
                            <td>{{ $item->bairro }}</td>
                            <td>{{ $item->cidade }}</td>
                            <td>{{ $item->telefone1 }}</td>
                            <td>
                                <a href="{{ url('/clientes/' . $item->id) }}" title="Visualizar cliente">
                                    <button class="btn btn-info btn-ms">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! $clientes->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
@endsection
