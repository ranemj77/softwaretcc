@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

@include('templates.headerClientes')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Cadastros";
}
.messageSubHeader:after
{
    content: "Mostrar Cliente";
}
@media only screen and (max-width: 445px)
{
    .btn
    {
        margin-bottom: 5px;
    }
}
</style>

<div class="box box-warning">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Detalhes do Cliente {{ $cliente->nome }}</h3></div>
                    <div class="card-body">

                        <a href="#" title="Voltar"><button class="btn btn-warning btn-ms" onclick="pageBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <a href="{{ url('/clientes/' . $cliente->id . '/edit') }}" title="Edit cliente"><button class="btn btn-primary btn-ms"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>

                        <form method="POST" action="{{ url('clientes' . '/' . $cliente->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-ms" title="Deletar cliente" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Deletar</button>
                        </form>
                        <a href="{{route('PDFmovimentocliente', $cliente->id)}}" title="Emitir Relatorio">
                            <button class="btn btn-success btn-ms"><i class="fa fa-download" aria-hidden="true"></i>
                                <span>Emitir Relatório</span>
                            </button>
                        </a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table">
                                <tbody>
                                    <tr>
                                        <th>Código</th>
                                        <td>{{ $cliente->codigo }}</td>
                                    </tr>
                                    <tr>
                                      <th> Nome </th>
                                      <td> {{ $cliente->nome }} </td>
                                    </tr>
                                    <tr>
                                      <th> Logradouro </th>
                                      <td> {{ $cliente->logradouro }} </td>
                                    </tr>
                                    <tr>
                                      <th> Número </th>
                                      <td> {{ $cliente->numero }} </td>
                                    </tr>
                                    <tr>
                                      <th> Bairro </th>
                                      <td> {{ $cliente->bairro }} </td>
                                    </tr>
                                    <tr>
                                      <th> Cidade </th>
                                      <td> {{ $cliente->cidade }} </td>
                                    </tr>
                                    <tr>
                                      <th> Estado </th>
                                      <td> {{ $cliente->estado }} </td>
                                    </tr>
                                    <tr>
                                      <th> CEP </th>
                                      <td> {{ $cliente->cep }} </td>
                                    </tr>
                                    <tr>
                                      <th> Telefone Principal </th>
                                      <td> {{ $cliente->telefone1 }} </td>
                                    </tr>
                                    <tr>
                                      <th> Outro Telefone </th>
                                      <td> {{ $cliente->telefone2 }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <h3> Ultimos movimentos com este cliente</h3>
                        @include('templates.tableMovimentos')

                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
