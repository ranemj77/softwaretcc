<form method="POST" action="{{ url('/fornecedores' . '/' . $item->id) }}"
      accept-charset="UTF-8" style="display:inline">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-danger btn-xs"
            title="Delete fornecedore"
            onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                class="fa fa-trash-o" aria-hidden="true"></i>
    </button>
</form>