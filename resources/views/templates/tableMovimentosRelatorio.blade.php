<style>
#movimentos {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    border-collapse: collapse;
    width: 100%;
}

#movimentos td, #movimentos th {
    border: 1px solid #ddd;
    padding: 8px;
}

#movimentos tr:nth-child(even){background-color: #f2f2f2;}

#movimentos tr:hover {background-color: #ddd;}

#movimentos th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: #6666ff;
    color: white;
}
</style>
<table id="movimentos">
      <tbody>
      <tr>
          <th>Código</th>
          <th>Data</th>
          <th>Fornecedor</th>
          <th>Cliente</th>
          <th>Produto</th>
          <th>Tipo</th>
          <th>Motivo</th>
          <th>Qt</th>
          <th>V. Unit</th>
          <th>V. Total</th>
      </tr>
      @foreach($movimentos as $item)
          <tr>
              <td>{{ $item->codigo }}</td>
              <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
              <td>{{ $item->fornecedor_nome}}</td>
              <td>{{ $item->cliente_nome}}</td>
              <td>{{ $item->produto_nome}}</td>
              <td>{{ $item->tipo}}</td>
              <td>{{ $item->motivo}}</td>
              <td>{{ $item->quantidade}}</td>
              <td>R$ {{ number_format($item->valor_unit, 2)}}</td>
              <td>R$ {{ number_format($item->valor_total, 2)}}</td>
          </tr>
          @endforeach
      </tbody>
</table>
