<div class="form-group {{ $errors->has('produto_id') ? 'has-error' : ''}}">
    <label for="produto_id" class="col-md-4 control-label">{{ 'Produto' }}</label>
    <div class="col-md-6">
        <select name="produto_id" class="form-control" id="produto_id" required>
            <option value="0">SELECIONE</option>
            @foreach (\App\produtos::all('id', 'nome') as $produto)
                <option value="{{ $produto->id }}" {{ (isset($produto->produto_id) && $produto->produto_id == $produto->produto_id) ? 'selected="selected"' : ''}}>{{ $produto->nome }}</option>
            @endforeach
        </select>
        {!! $errors->first('produto_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>