<div class="form-group {{ $errors->has('quantidade') ? 'has-error' : ''}}">
    <label for="valor_unit" class="col-md-4 control-label">{{ 'Valor Unitario R$' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="valor_unit" type="number" step="0.01" id="quantidade" min="1" value="{{ $movimento->valor_unit or ''}}" >
        {!! $errors->first('valor_unit', '<p class="help-block">:message</p>') !!}
    </div>
</div>
