<style>
#posicao {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    border-collapse: collapse;
    width: 100%;
}

#posicao td, #posicao th {
    border: 1px solid #ddd;
    padding: 8px;
}

#posicao tr:nth-child(even){background-color: #f2f2f2;}

#posicao tr:hover {background-color: #ddd;}

#posicao th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: #6666ff;
    color: white;
}
</style>
<table id="posicao">
      <tbody>
      <tr>
          <th width="45">Código</th>
          <th width="100">Nome</th>
          <th>Marca</th>
          <th>Categoria</th>
          <th width="70">Quantidade</th>
          <th width="70">Custo Médio</th>
          <th width="70">Valor Total</th>
      </tr>
      @foreach ($produtos as $item)
          <tr>
              <td>{{ $item->codigo }}</td>
              <td>{{ $item->nome }}</td>
              <td>{{ $item->marca }}</td>
              <td>{{ $item->categoria }}</td>
              <td>{{ $item->quantidade }}</td>
              <td>R$ {{ number_format($item->custo_medio, 2)}}</td>
              <td>R$ {{ number_format($item->valor_total, 2)}}</td>
          </tr>
      @endforeach
      </tbody>
  </table>
</div>
