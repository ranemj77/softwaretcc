@if (session('success_message'))
    <div class="alert alert-success" id="alert-msg">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        {{ session('success_message') }}</div>
@elseif (session('warning_message'))
    <div class="alert alert-warning" id="alert-msg">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        {{ session('warning_message') }}</div>
@elseif (session('error_message'))
    <div class="alert alert-error" id="alert-msg">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        {{ session('error_message') }}</div>
@endif
