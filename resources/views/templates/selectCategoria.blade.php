<div class="form-group {{ $errors->has('categoria_id') ? 'has-error' : ''}}">
    <label for="categoria_id" class="col-md-4 control-label">{{ 'Categoria' }}</label>
    <div class="col-md-6">
        <select name="categoria_id" class="form-control" id="categorias_id" required>
            <option value="0">SELECIONE</option>
            @foreach (\App\categorias::all('id', 'nome') as $categoria)
                <option value="{{ $categoria->id }}" {{ (isset($categoria->categoria_id) && $categoria->categoria_id == $categoria->id) ? 'selected="selected"' : ''}}>{{ $categoria->nome }}</option>
            @endforeach
        </select>
        {!! $errors->first('categoria_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
