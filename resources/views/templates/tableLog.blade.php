<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tbody>
        <tr>
            <th>Código</th>
            <th>Acesso em:</th>
            <th>IP da máquina</th>
            <th>Nome da maquina</th>
        </tr>
        <tr>
        @foreach($log as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                <td>{{ $item->last_login_ip}}</td>
                <td>{{ $item->last_login_hostname}}</td>
            </tr>
            @endforeach
            </tr>
        </tbody>
    </table>
    <div class="pagination-wrapper"> {!! $log->appends(['search' => Request::get('search')])->render() !!} </div>
</div>
