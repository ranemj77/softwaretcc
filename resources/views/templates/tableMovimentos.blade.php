<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tbody>
        <tr>
            <th>Código</th>
            <th>Data de criação</th>
            <th>Fornecedor</th>
            <th>Cliente</th>
            <th>Produto</th>
            <th>Tipo</th>
            <th>Motivo</th>
            <th>Quantidade</th>
            <th>Valor Unitario</th>
            <th>Valor Total</th>
            <th>Visualizar</th>
        </tr>
        <tr>
        @foreach($movimentos as $item)
            <tr>
                <td>{{ $item->codigo }}</td>
                <td>{{ $item->created_at->format('d/m/Y H:i:s') }}</td>
                <td>{{ $item->fornecedor_nome}}</td>
                <td>{{ $item->cliente_nome}}</td>
                <td>{{ $item->produto_nome}}</td>
                <td>{{ $item->tipo}}</td>
                <td>{{ $item->motivo}}</td>
                <td>{{ $item->quantidade}}</td>
                <td>R$ {{ number_format($item->valor_unit, 2)}}</td>
                <td>R$ {{ number_format($item->valor_total, 2)}}</td>
                <td>
                    <a href="{{ url('/movimentos/' . $item->id) }}" title="Visualizar movimento">
                        <button class="btn btn-info btn-ms"><i class="fa fa-eye" aria-hidden="true"></i>
                        </button>
                    </a>
                </td>
            </tr>
            @endforeach
            </tr>
        </tbody>
    </table>
    <div class="pagination-wrapper"> {!! $movimentos->appends(['search' => Request::get('search')])->render() !!} </div>
</div>
