<div class="form-group {{ $errors->has('cliente_id') ? 'has-error' : ''}}">
    <label for="cliente_id" class="col-md-4 control-label">{{ 'Cliente' }}</label>
    <div class="col-md-6">
        <select name="cliente_id" class="form-control" id="clientes_id" required>
            <option value="0">SELECIONE</option>
            @foreach (\App\clientes::all('id', 'nome') as $cliente)
                <option value="{{ $cliente->id }}" {{ (isset($cliente->cliente_id) && $cliente->cliente_id == $cliente->id) ? 'selected="selected"' : ''}}>{{ $cliente->nome }}</option>
            @endforeach
        </select>
        {!! $errors->first('cliente_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>