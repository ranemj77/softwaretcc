<div class="form-group {{ $errors->has('fornecedor_id') ? 'has-error' : ''}}">
    <label for="fornecedor_id" class="col-md-4 control-label">{{ 'Fornecedor' }}</label>
    <div class="col-md-6">
        <select name="fornecedor_id" class="form-control" id="fornecedores_id" required>
            <option value="0">SELECIONE</option>
            @foreach (\App\fornecedores::all('id', 'nome') as $fornecedor)
                <option value="{{ $fornecedor->id }}" {{ (isset($fornecedor->fornecedor_id) && $fornecedor->fornecedor_id == $fornecedor->id) ? 'selected="selected"' : ''}}>{{ $fornecedor->nome }}</option>
            @endforeach
        </select>
        {!! $errors->first('fornecedor_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>