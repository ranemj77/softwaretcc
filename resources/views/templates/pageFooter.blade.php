<script type="text/php">
    if (isset($pdf))
	{
        $x = 250;
        $y = 740;
        $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
        $font = 'Arial';
        $size = 14;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
