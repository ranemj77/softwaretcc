<div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>Código</th>
                <th>Nome</th>
                <th>Marca</th>
                <th>Categoria</th>
                <th>Quantidade</th>
                <th>Custo Médio</th>
                <th>Valor Total</th>
                <th>Visualizar</th>
            </tr>
            @foreach ($produtos as $item)
                <tr>
                    <td>{{ $item->codigo }}</td>
                    <td>{{ $item->nome }}</td>
                    <td>{{ $item->marca }}</td>
                    <td>{{ $item->categoria_nome }}</td>
                    <td>{{ $item->quantidade }}</td>
                    <td>R$ {{ number_format($item->custo_medio, 2)}}</td>
                    <td>R$ {{ number_format($item->valor_total, 2)}}</td>
                    <td>
                        <a href="{{ url('/produtos/' . $item->id) }}" title="Visualizar produto">
                            <button class="btn btn-info btn-ms">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination-wrapper"> {!! $produtos->appends(['search' => Request::get('search')])->render() !!} </div>
    </div>
