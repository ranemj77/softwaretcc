<div class="form-group {{ $errors->has('tipo') ? 'has-error' : ''}}">
    <div class="col-md-6">
        <input name="tipo" type="hidden" id="tipo" value="{{ $movimento->tipo or 'saida'}}" >
        {!! $errors->first('tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
