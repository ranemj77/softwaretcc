@extends('adminlte::page')

@section('content')

@include('templates.headerUsers')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Administração";
}
.messageSubHeader:after
{
    content: "Mostrar Usuario";
}
@media only screen and (max-width: 445px)
{
    .btn
    {
        margin-bottom: 5px;
    }
}
</style>

<div class="box box-info">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Detalhes do Usuário {{$user->name}}</h3></div>
                    <div class="card-body">

                        <a href="#" title="Voltar"><button class="btn btn-warning btn-ms" onclick="pageBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Editar Usuario"><button class="btn btn-primary btn-ms"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/users', $user->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-ms',
                                    'title' => 'Delete User',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <a href="{{route('PDFmovimentousuario', $user->id)}}" title="Emitir Relatorio">
                            <button class="btn btn-success btn-ms"><i class="fa fa-download" aria-hidden="true"></i>
                                <span>Emitir Relatório</span>
                            </button>
                        </a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Código</th>
                                        <td>{{ $user->codigo }}</td>
                                    </tr>
                                    <tr>
                                        <th>Nome do usuário</th>
                                        <td> {{ $user->name }} </td>
                                    </tr>
                                    <tr>
                                        <th>E-mail do usuário</th>
                                        <td> {{ $user->email }} </td>
                                    </tr>
                                    <tr>
                                        <th>Ultimo Acesso</th>
                                        <td> {{ $user->last_login_at_format }} </td>
                                    </tr>
                                    <tr>
                                        <th>IP do ultimo acesso</th>
                                        <td> {{ $user->last_login_ip }} </td>
                                    </tr>
                                    <tr>
                                        <th>Nome da máquina do ultimo acesso</th>
                                        <td> {{ $user->last_login_hostname }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <h3> Registro de logins </h3>
                        @include('templates.tableLog')
                        <h3> Ultimos movimentos feitos por este usuário </h3>
                        @include('templates.tableMovimentos')
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
