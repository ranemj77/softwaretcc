@extends('adminlte::page')

@section('content')

@include('templates.headerUsers')

@include('templates.alerts')

<style>
.messageHeader:after
{
    content: "Administração";
}
.messageSubHeader:after
{
    content: "Criar Usuario";
}
</style>

<div class="box box-info">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><h3>Criar novo usuário</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/admin/users') }}" title="Back">
                            <button class="btn btn-warning btn-ms">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar
                            </button>
                        </a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/users', 'class' => 'form-horizontal']) !!}

                        @include ('admin.users.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
