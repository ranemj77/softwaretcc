@extends('adminlte::page')

@section('content')

@include('templates.headerUsers')

@include('templates.alerts')

<style>
.box-header>.box-tools
{
    right: 12px !important;
    top: 9px !important;
}
.btn-padding
{
    padding-left: 5px;
}
.messageHeader:after
{
    content: "Administração";
}
.messageSubHeader:after
{
    content: "Visualizar Usuarios";
}
</style>

  <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">
                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-ms" title="Add New User">
                  <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
                </a>
              </h3>

              <div class="box-tools">
                <div class="input-group input-group-sm">
                  {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                  <div class="input-group">
                      <input type="text" class="form-control" name="search" placeholder="Procurar...">
                      <div class="input-group-btn btn-padding">
                          <button class="btn btn-secondary" type="submit">
                              <i class="fa fa-search"></i> Buscar
                          </button>
                      </div>
                  </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Código</th>
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Ultimo Acesso</th>
                  <th>Visualizar</th>
                </tr>
                @foreach($users as $item)
                    <tr>
                        <td>{{ $item->codigo }}</td>
                        <td>
                          <a href="{{ url('/admin/users', $item->id) }}">{{ $item->name }}</a>
                        </td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->last_login_at_format}}</td>
                        <td>
                            <a href="{{ url('/admin/users/' . $item->id) }}" title="View User">
                            <button class="btn btn-info btn-ms"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
