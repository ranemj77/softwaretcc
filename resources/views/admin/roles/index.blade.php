@extends('adminlte::page')

@section('content')

@include('templates.headerRoles')

@include('templates.alerts')

<style>
.box-header>.box-tools
{
    right: 12px !important;
    top: 9px !important;
}
.messageHeader:after
{
    content: "Administração";
}
.messageSubHeader:after
{
    content: "Visualizar Papeis";
}
</style>

  <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">
                <a href="{{ url('/admin/roles/create') }}" class="btn btn-success btn-ms" title="Add New Role">
                    <i class="fa fa-plus" aria-hidden="true"></i> Adicionar
                </a>
              </h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  {!! Form::open(['method' => 'GET', 'url' => '/admin/roles', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                  <div class="input-group">
                      <input type="text" class="form-control" name="search" placeholder="Procurar...">
                      <div class="input-group-btn">
                          <button class="btn btn-secondary" type="submit">
                              <i class="fa fa-search"></i>
                          </button>
                      </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                  <tr>
                      <th>Código</th>
                      <th>Nome</th>
                      <th>Rótulo</th>
                      <th>Visualizar</th>
                  </tr>
                  @foreach($roles as $item)
                      <tr>
                          <td>{{ $item->codigo}}</td>
                          <td><a href="{{ url('/admin/roles', $item->id) }}">{{ $item->name }}</a></td><td>{{ $item->label }}</td>
                          <td>
                              <a href="{{ url('/admin/roles/' . $item->id) }}" title="View Role"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                          </td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $roles->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
