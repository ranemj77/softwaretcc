@extends('adminlte::page')

@section('title', 'DataFibra ')

@section('content')

<style>
.btn-padding
{
    padding-left: 5px;
}
@media only screen and (max-width: 490px)
{
    .box-header>.box-tools
    {
        position: static !important;
        margin-top: 10px;
    }
    .textSearch
    {
        width: 100%;
    }
}
</style>

<div class="small-box bg-green">
    <div class="inner">
        <h3>Estoque Atual</h3>

        <p>Detalhes do Estoque</p>
    </div>
    <div class="icon">
        <i class="ion ion-stats-bars"></i>
    </div>
</div>


@include('templates.alerts')

<div class="box box-success">
    <div class="box-header">
        <a href="{{ route('PDFposicaoestoque') }}" class="btn btn-success btn-ms" title="Emitir Relatório">
            <i class="fa fa-download" aria-hidden="true"></i> <span> Emitir Relatório</span>
        </a>


    <div class="box-tools">
            <form method="GET" action="{{ url('/') }}" accept-charset="UTF-8"
                  class="form-inline my-2 my-lg-0 float-right" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search" placeholder="Procurar..."
                           value="{{ request('search') }}">
                    <div class="input-group-btn btn-padding">
                        <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i> Buscar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<!-- /.box-header -->
@include('templates.tablePosicao')
<!-- /.box-body -->
</div>
<!-- /.box -->
@stop
