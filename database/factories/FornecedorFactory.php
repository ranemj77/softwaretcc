<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Fornecedores::class,  function (Faker $faker) {
    return [
        'nome'          => $faker->name,
        'logradouro'    => $faker->address,
        'bairro'        => $faker->word,
        'cep'           => $faker->numberBetween(10000000, 99999999),
        'cidade'        => $faker->city,
        'telefone1'     => $faker->phoneNumber,
        'telefone2'     => $faker->phoneNumber,
    ];
});
