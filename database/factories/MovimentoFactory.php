<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Movimentos::class,  function (Faker $faker) {
    $fornecedor     = App\Fornecedores::all()->pluck('id')->toArray();
    $cliente        = App\Clientes::all()->pluck('id')->toArray();
    $produto        = App\Produtos::all()->pluck('id')->toArray();
    $idproduto      = $faker->randomElement($produto);
    $tipo           = 'saida';
    $quantidade     = $faker->numberBetween(1, 100);
    $valorUnitario  = $faker->numberBetween(5, 200);

    $produtoSelecionado = App\Produtos::findOrFail($idproduto);

    if ($tipo == 'entrada')
    {
        $valorTotal                         = $quantidade * $valorUnitario;
        $produtoSelecionado->quantidade     = $produtoSelecionado->quantidade + $quantidade;
        $produtoSelecionado->valor_total    = $produtoSelecionado->valor_total + $valorTotal;
        $produtoSelecionado->custo_medio    = $produtoSelecionado->valor_total/$produtoSelecionado->quantidade;
        $produtoSelecionado->save();
        return
        [
            'fornecedor_id'     => $faker->randomElement($fornecedor),
            'produto_id'        => $idproduto,
            'tipo'              => $tipo,
            'motivo'            => $faker->word,
            'descricao'         => $faker->text,
            'quantidade'        => $quantidade,
            'valor_unit'        => $valorUnitario,
            'valor_total'       => $valorTotal,
        ];
    }
    else
    {
        $valorTotal     = $produtoSelecionado->custo_medio * $quantidade;
        if ($quantidade > 0 && $produtoSelecionado->quantidade >= $quantidade)
        {
            $produtoSelecionado->quantidade     = $produtoSelecionado->quantidade - $quantidade;
            $produtoSelecionado->valor_total    = $produtoSelecionado->valor_total - $valorTotal;
            if ($produtoSelecionado->quantidade - $quantidade == 0)
            {
                $produtoSelecionado->custo_medio =  0;
            }
            else
            {
                $produtoSelecionado->custo_medio = $produtoSelecionado->valor_total / $produtoSelecionado->quantidade;
            }
            $produtoSelecionado->save();
            return
            [
                'cliente_id'    => $faker->randomElement($cliente),
                'produto_id'    => $idproduto,
                'tipo'          => $tipo,
                'motivo'        => $faker->word,
                'descricao'     => $faker->text,
                'quantidade'    => $quantidade,
                'valor_unit'    => $produtoSelecionado->custo_medio,
                'valor_total'   => $valorTotal,
            ];
        }
        else
        {
            $valorTotal                         = $quantidade * $valorUnitario;
            $produtoSelecionado->quantidade     = $produtoSelecionado->quantidade + $quantidade;
            $produtoSelecionado->valor_total    = $produtoSelecionado->valor_total + $valorTotal;
            $produtoSelecionado->custo_medio    = $produtoSelecionado->valor_total/$produtoSelecionado->quantidade;
            $produtoSelecionado->save();
            return
            [
                'fornecedor_id'     => $faker->randomElement($fornecedor),
                'produto_id'        => $idproduto,
                'tipo'              => 'entrada',
                'motivo'            => $faker->word,
                'descricao'         => $faker->text,
                'quantidade'        => $quantidade,
                'valor_unit'        => $valorUnitario,
                'valor_total'       => $valorTotal,
            ];
        }
    }
});
