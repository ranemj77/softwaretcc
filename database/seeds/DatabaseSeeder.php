<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ClienteSeeder::class);
        //$this->call(FornecedorSeeder::class);
        //$this->call(ProdutoSeeder::class);
        $this->call(UserRoleSeeder::class);
        //$this->call(MovimentoTableSeeder::class);
    }
}
