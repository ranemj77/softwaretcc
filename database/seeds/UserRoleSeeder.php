<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\User::class, 1)->create();
      factory(App\Role::class, 1)->create([
          'name'  => 'admin',
          'label' => 'Administrador',
      ]);
      factory(App\Role::class, 1)->create([
          'name'  => 'gerente',
          'label' => 'Gerente',
      ]);
      factory(App\Role::class, 1)->create([
          'name'  => 'funcionario',
          'label' => 'Funcionario',
      ]);
      $roles = App\Role::all();
      App\User::all()->each(function ($user) use ($roles) {
          $user->roles()->attach(
              $roles->where('name', 'LIKE', "admin")->pluck('id')->toArray()
          );
      });
    }
}
