<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimentos', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->integer('fornecedor_id')->unsigned()->nullable();
            $table->integer('cliente_id')->unsigned()->nullable();
            $table->integer('produto_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->enum('tipo', ['entrada', 'saida']);
            $table->string('motivo');
            $table->text('descricao');
            $table->integer('quantidade')->unsigned()->default(0);
            $table->float('valor_unit')->unsigned()->default(0);
            $table->float('valor_total')->unsigned()->default(0);
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores')->onUpdate('cascade');
            $table->foreign('cliente_id')->references('id')->on('clientes')->onUpdate('cascade');
            $table->foreign('produto_id')->references('id')->on('produtos')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movimentos');
    }
}
