<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('categoria_id')->unsigned();
            $table->timestamps();
            $table->string('nome')->unique();
            $table->string('marca');
            $table->text('descricao');
            $table->integer('quantidade')->unsigned()->default(0);
            $table->float('custo_medio')->unsigned()->default(0);
            $table->float('valor_total')->unsigned()->default(0);
            $table->foreign('categoria_id')->references('id')->on('categorias')->onUpdate('cascade');
            $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
    }
}
