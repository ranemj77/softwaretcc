<?php

namespace App\Listeners;

use App\Events\Event;
use App\loginLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request              = $request;
        $user                       = Auth::user();
        $user->last_login_at        = new \DateTime();
        $user->last_login_ip        = $this->request->ip();
        $user->last_login_hostname  = gethostbyaddr($this->request->ip());
        $user->save();
        loginLog::create
        ([
            'user_id'               => $user->id,
            'last_login_ip'         => $user->last_login_ip,
            'last_login_hostname'   => $user->last_login_hostname,
        ]);
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //
    }
}
