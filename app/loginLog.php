<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class loginLog extends Model
{
    use softDeletes;

    protected $table = 'login_log';

    protected $fillable = ['user_id', 'last_login_at', 'last_login_ip', 'last_login_hostname'];

}
