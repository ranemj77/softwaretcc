<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class movimentos extends Model
{
    use softDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'movimentos';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['fornecedor_id', 'cliente_id', 'produto_id', 'user_id', 'tipo', 'motivo', 'descricao', 'quantidade', 'valor_unit', 'valor_total'];

    public function fornecedores(){
        return $this->belongsTo('App\fornecedores');
    }

}
