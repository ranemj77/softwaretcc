<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class fornecedores extends Model
{
    use softDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fornecedores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'cep', 'logradouro', 'numero', 'bairro', 'cidade', 'estado', 'telefone1', 'telefone2'];

    public function movimentos(){
        return $this->hasMany('App\movimentos');
    }

}
