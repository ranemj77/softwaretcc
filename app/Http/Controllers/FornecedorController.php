<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\fornecedores;
use App\movimentos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $fornecedores = fornecedores::where('id', 'like', '%' . $keyword . '%')
                                        ->orWhere('nome', 'like', '%' . $keyword . '%')
                                        ->orWhere('logradouro', 'like', '%' . $keyword . '%')
                                        ->orWhere('bairro', 'like', '%' . $keyword . '%')
                                        ->orWhere('cidade', 'like', '%' . $keyword . '%')
                                        ->paginate($perPage);
        } else {
            $fornecedores = fornecedores::paginate($perPage);
        }
        foreach ($fornecedores as $fornecedorKey => $fornecedor)
        {
            $fornecedores[$fornecedorKey]->codigo = str_pad($fornecedor->id, 3, 0, STR_PAD_LEFT);
        }
        return view('fornecedores.index', compact('fornecedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('fornecedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try
        {
            $requestData = $request->all();
            fornecedores::create($requestData);
            return redirect('fornecedores')->with('sucess_message', 'Fornecedor adicionado com sucesso!');
        }
        catch (QueryException $e)
        {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062)
            {
                return redirect()->back()->with('error_message', 'O fornecedor já existe!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
            $fornecedor = fornecedores::findOrFail($id);
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->where('movimentos.fornecedor_id', 'like', $id)
                                    ->orderBy('created_at', 'desc')->paginate(5);

            $fornecedor->codigo = str_pad($fornecedor->id, 3, 0, STR_PAD_LEFT);
            foreach ($movimentos as $movimentKey => $movimento)
            {
                $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
                if ($movimento->fornecedor_nome == '')
                {
                    $movimento->fornecedor_nome = '-';
                }
                else
                {
                    $movimento->cliente_nome = '-';
                }
            }
            return view('fornecedores.show', compact('fornecedor', 'movimentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public
    function edit($id)
    {
        $fornecedore = fornecedores::findOrFail($id);

        return view('fornecedores.edit', compact('fornecedore'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public
    function update(Request $request, $id)
    {

        $requestData = $request->all();

        $fornecedore = fornecedores::findOrFail($id);
        $fornecedore->update($requestData);

        return redirect('fornecedores')->with('success_message', 'Forncedor alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public
    function destroy($id)
    {
        fornecedores::destroy($id);

        return redirect('fornecedores')->with('success_message', 'Forncedor deletado com sucesso!');
    }

    public
    function listJson()
    {
        $fornecedor = Fornecedor::all();
        return response()->json($fornecedor);
    }
}
