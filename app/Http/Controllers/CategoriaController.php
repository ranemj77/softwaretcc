<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\categorias;
use App\movimentos;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $categorias = categorias::paginate($perPage);
        } else {
            $categorias = categorias::paginate($perPage);
        }
        foreach ($categorias as $categoriaKey => $categoria)
        {
            $categorias[$categoriaKey]->codigo = str_pad($categoria->id, 3, 0, STR_PAD_LEFT);
        }
        return view('categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try
        {
            $requestData = $request->all();
            categorias::create($requestData);
            return redirect('categorias')->with('success_message', 'Categoria adicionada com sucesso!');
        }
        catch (QueryException $e)
        {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062)
            {
                return redirect()->back()->with('error_message', 'A categoria já existe!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $categoria = categorias::findOrFail($id);
        $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                                ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'categorias.nome as categoria_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->where('categorias.id', 'like', $id)
                                ->orderBy('created_at', 'asc')
                                ->orderBy('id', 'asc')
                                ->paginate(5);

        $categoria->codigo = str_pad($categoria->id, 3, 0, STR_PAD_LEFT);
        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }

        return view('categorias.show', compact('categoria', 'movimentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $categoria = categorias::findOrFail($id);

        return view('categorias.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $categoria = categorias::findOrFail($id);
        $categoria->update($requestData);

        return redirect('categorias')->with('success_message', 'Categoria atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        categorias::destroy($id);

        return redirect('categorias')->with('success_message', 'Categoria deletada com sucesso!');
    }
}
