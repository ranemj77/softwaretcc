<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\movimentos;
use App\produtos;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;

class MovimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;
        if (!empty($keyword))
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                     ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                     ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                     ->select('movimentos.id', 'users.name as user_name', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                     ->where('movimentos.id', 'like', '%'.$keyword.'%')
                                     ->orWhere('fornecedores.nome', 'like', '%'.$keyword.'%')
                                     ->orWhere('clientes.nome', 'like', '%'.$keyword.'%')
                                     ->orWhere('produtos.nome', 'like', '%'.$keyword.'%')
                                     ->orWhere('movimentos.tipo', 'like', '%'.$keyword.'%')
                                     ->orWhere('movimentos.motivo', 'like', '%'.$keyword.'%')
                                     ->orWhere('movimentos.created_at', 'like', '%'.$keyword.'%')
                                     ->orWhere('movimentos.valor_unit', 'like', '%'.$keyword.'%')
                                     ->orWhere('movimentos.valor_total', 'like', '%'.$keyword.'%')
                                     ->orderBy('created_at', 'asc')
                                     ->orderBy('id', 'asc')
                                     ->paginate($perPage);
        }
        else
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->paginate($perPage);
        }
        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }

        return view('movimentos.index', compact('movimentos'));
    }

    public function getFornecedores()
    {
        $fornecedores = DB::table('fornecedores')->pluck('id', 'nome');
        return $fornecedores;
    }

    public function getClientes()
    {
        $clientes = DB::table('clientes')->pluck('id', 'nome');
        return $clientes;
    }

    public function getProdutos()
    {
        $produtos = DB::table('produtos')->pluck('id', 'nome');
        return $produtos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function entrada()
    {
        return view('movimentos.entrada');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function saida()
    {
        return view('movimentos.saida');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        /*
        return view('movimentos.create');
        */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user                   = Auth::user()->id;
        $requestData            = $request->all();
        $requestData['user_id'] = $user;
        $requestQuantidade      = (int) $request->input('quantidade');
        $requestId              = $request->input('produto_id');
        $requestTipo            = $request->input('tipo');
        $requestValorUnit       = (int) $request->input('valor_unit');
        $produto                = produtos::findOrFail($requestId);
        $produtoQuantidade      = (int) $produto->quantidade;
        $produtoValorTotal      = (float) $produto->valor_total;
        $produtoCustoMedio      = (float) $produto->custo_medio;
        //dd($requestData['valor_total'], $produtoCustoMedio, $ValorTotal, $requestTipo);
        if ($requestTipo == 'saida')
        {
            $ValorTotal                 = $produtoCustoMedio * $requestQuantidade;
            $requestData['valor_total'] = $ValorTotal;
            if (($produtoValorTotal - $ValorTotal) < 0)
            {
                return redirect('movimentos')->with('error_message', 'Houve um erro inesperado!');
            }
            else
            {
                if ($requestQuantidade > 0 && $produtoQuantidade >= $requestQuantidade)
                {
                  $requestData['valor_unit']    =  $produtoCustoMedio;
                  $produto->quantidade          = ($produtoQuantidade - $requestQuantidade);
                  $produto->valor_total         = ($produtoValorTotal - $ValorTotal);
                  if (($produtoQuantidade - $requestQuantidade) == 0)
                  {
                      $produto->custo_medio = 0;
                  }
                  else
                  {
                      $produto->custo_medio = (($produtoValorTotal - $ValorTotal)/($produtoQuantidade - $requestQuantidade));
                  }
                  $produto->save();
                  movimentos::create($requestData);
                  return redirect('movimentos')->with('success_message', 'Saída registrada com sucesso!');
              }
              elseif ($produtoQuantidade < $requestQuantidade)
              {
                  return redirect('movimentos')->with('warning_message', 'Quantidade insuficiente em estoque!');
              }
              else
              {
                  return redirect('movimentos')->with('error_message', 'Houve um erro inesperado!');
              }
          }
        }
        else
        {
            $ValorTotal                 = $requestValorUnit * $requestQuantidade;
            $requestData['valor_total'] = $ValorTotal;
            if ($requestQuantidade > 0)
            {
                $produto->quantidade    = ($produtoQuantidade + $requestQuantidade);
                $produto->valor_total   = ($produtoValorTotal + $ValorTotal);
                $produto->custo_medio   = (($produtoValorTotal + $ValorTotal)/($produtoQuantidade + $requestQuantidade));
                $produto->save();
                movimentos::create($requestData);
                return redirect('movimentos')->with('success_message', 'Entrada registrada com sucesso!');
            }
            else
            {
                return redirect('movimentos')->with('warning_message', 'Quantidade da entrada deve ser maior que zero.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $movimento = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->leftjoin('users', 'movimentos.user_id', 'like', 'users.id')
                                ->select('movimentos.id', 'users.name as user_name', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.descricao', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->findOrFail($id);
        $movimento->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
        if ($movimento->fornecedor_nome == '')
        {
            $movimento->fornecedor_nome = '-';
        }
        else
        {
            $movimento->cliente_nome = '-';
        }
        return view('movimentos.show', compact('movimento'));

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        /*
        $movimento = movimentos::findOrFail($id);

        return view('movimentos.edit', compact('movimento'));
        */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
      /*
        $requestData = $request->all();

        $movimento = movimentos::findOrFail($id);
        $movimento->update($requestData);

        return redirect('movimentos')->with('success_message', '');
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
      /*
        movimentos::destroy($id);

        return redirect('movimentos')->with('success_message', '');
        */
    }

    public function generatePDF($id){
      $movimento    = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->leftjoin('users', 'movimentos.user_id', 'like', 'users.id')
                                ->select('movimentos.id', 'users.name as user_name', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.descricao', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->findOrFail($id);
      $user         = Auth::user()->name;
      $myTime       = new \DateTime();
      $data         =
      [
          'title'           => 'Impressão do Movimento de Estoque',
          'footer'          => 'DataFibra Estoque Fácil',
          'version'         => 'Versão 1.0.0',
          'current_user'    => $user,
          'cadastrante'     => $movimento->user_name,
          'footerDate'      => $myTime->format('d/m/Y H:i:s'),
          'img_path'        => '..\public\images\DataFibra.png',
          'movimento'       => $movimento
      ];
      if ($movimento->fornecedor_nome == '')
      {
          $movimento->fornecedor_nome = '-';
      }
      else
      {
          $movimento->cliente_nome = '-';
      }
      $movimento->codigo    = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
      $pdf                  = PDF::loadView('relatorios/movimento', $data);

      return $pdf->download('impressaoMovimento'.$movimento->fornecedor_nome.''.$movimento->cliente_nome.''.$movimento->tipo.'.pdf');
    }
}
