<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\movimentos;
use App\fornecedores;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;

class MovimentoFornecedorController extends Controller
{
    public function generatePDF($id)
    {
        $fornecedor     = fornecedores::findOrFail($id);
        $user           = Auth::user()->name;
        $myTime         = new \DateTime();

        if (!empty($fornecedor->nome))
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->where('movimentos.tipo', 'like', 'entrada')
                                    ->where('fornecedores.id', 'like', $id)
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->get();
            $count = count($movimentos);
            if ($count == 0)
            {
                return redirect()->back()->with('error_message', 'Não possivel emitir o relatório! O fornecedor não possui movimentação!');
            }
        }
        else
        {
           return redirect()->back()->with('error_message', 'Você precisa escolher pelo menos um fornecedor para emitir este relatório!');
        }

        $movimentacao = [];
        $valorEntradas = 0;
        foreach ($movimentos as $movimento)
        {
            $movimento->codigo = str_pad($movimento->id, 4, 0, STR_PAD_LEFT);
            array_push($movimentacao, $movimento);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
                $valorEntradas = $valorEntradas + $movimento->valor_total;
            }
        }
        $data =
        [
            'title'           => 'Relatório de Movimentações (Fornecedor)',
            'footer'          => 'DataFibra Estoque Fácil',
            'version'         => 'Versão 1.0.0',
            'current_user'    => $user,
            'movimentos'      => $movimentacao,
            'valorEntradas'   => $valorEntradas,
            'fornecedor'      => $fornecedor->nome,
            'footerDate'      => $myTime->format('d/m/Y H:i:s'),
            'img_path'        => '..\public\images\DataFibra.png'
        ];

        $pdf = PDF::loadView('relatorios/movimentoFornecedorRelatorio', $data);

        return $pdf->download('MovimentoFornecedor'.$fornecedor->nome.'.pdf');
    }
}
