<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\loginLog;
use App\movimentos;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $users = User::paginate($perPage);
        }
        foreach ($users as $userKey => $user)
        {
            $users[$userKey]->codigo = str_pad($user->id, 3, 0, STR_PAD_LEFT);
            if ($user->last_login_at != null)
            {
                $users[$userKey]->last_login_at_format = $user->last_login_at->format('d/m/Y H:i:s');
            }
            else
            {
                $user->last_login_at = null;
            }
        }
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        try
        {
            $this->validate($request, ['name' => 'required', 'email' => 'required', 'password' => 'required', 'roles' => 'required']);
            $data = $request->except('password');
            $data['password'] = bcrypt($request->password);
            $user = User::create($data);
        }
        catch (QueryException $e)
        {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062)
            {
                return redirect()->back()->with('error_message', 'O usuário já existe!');
            }
        }
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        return redirect('admin/users')->with('success_message', 'Usuario adicionado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $log = loginLog::leftjoin('users', 'login_log.user_id', 'like', 'users.id')
                        ->select('login_log.id', 'login_log.created_at', 'users.name', 'login_log.last_login_ip', 'login_log.last_login_hostname')
                        ->where('users.id', 'like', $id)
                        ->orderBy('created_at', 'desc')
                        ->paginate(5);

        $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->leftjoin('users', 'movimentos.user_id', 'like', 'users.id')
                                ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->where('users.id', 'like', $id)
                                ->orderBy('created_at', 'asc')
                                ->orderBy('id', 'asc')
                                ->paginate(5);

        $user->codigo = str_pad($user->id, 3, 0, STR_PAD_LEFT);
        if ($user->last_login_at != null)
        {
            $user->last_login_at_format = $user->last_login_at->format('d/m/Y H:i:s');
        }
        else
        {
            $user->last_login_at = null;
        }
        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }

        return view('admin.users.show', compact('user', 'movimentos', 'log'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $roles = Role::select('id', 'name', 'label')->get();
        $roles = $roles->pluck('label', 'name');

        $user = User::with('roles')->select('id', 'name', 'email')->findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }

        return view('admin.users.edit', compact('user', 'roles', 'user_roles'));
    }

    public function reset(Request $request){

        return view('admin.users.resetLogged');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required', 'roles' => 'required']);

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);

        $user->roles()->detach();
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        return redirect('admin/users')->with('success_message', 'Usuario alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/users')->with('success_message', 'Usuario deletado com sucesso!');
    }

}
