<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Auth;
use App\produtos;

use Illuminate\Http\Request;

class PosicaoDeEstoqueController extends Controller
{

    public function generatePDF(){
        $user               = Auth::user()->name;
        $myTime             = new \DateTime();
        $produtos           = produtos::all();
        $qtTotalProdutos    = 0;
        $valorTotalEstoque  = 0;
        if (count($produtos) == 0){
            return redirect()->back()->with('error_message', 'Não possivel emitir o relatório! Não há produtos no estoque!');
        }
        foreach ($produtos as $produtoKey => $produto)
        {
            $produtos[$produtoKey]->codigo = str_pad($produto->id, 3, 0, STR_PAD_LEFT);
        }
        $custoMedioEstoque = 0;
        $data =
        [
            'title'                  => 'Relatório de Posição de Estoque',
            'footer'                 => 'DataFibra Estoque Fácil',
            'version'                => 'Versão 1.0.0',
            'current_user'           => $user,
            'produtos'               => $produtos,
            'custoMedioEstoque'      => $custoMedioEstoque,
            'valorTotalEstoque'      => $valorTotalEstoque,
            'footerDate'             => $myTime->format('d/m/Y H:i:s'),
            'img_path'               => '..\public\images\DataFibra.png'
        ];

        $pdf = PDF::loadView('relatorios/posicaoDeEstoqueRelatorio', $data);

        return $pdf->download('RelatorioPosicao.pdf');
    }
}
