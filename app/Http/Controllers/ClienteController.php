<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\clientes;
use App\movimentos;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $clientes = clientes::where('id', 'like', '%'.$keyword.'%')
                                ->orWhere('nome', 'like', '%'.$keyword.'%')
                                ->orWhere('logradouro', 'like', '%'.$keyword.'%')
                                ->orWhere('bairro', 'like', '%'.$keyword.'%')
                                ->orWhere('cidade', 'like', '%'.$keyword.'%')
                                ->paginate($perPage);
        } else {
            $clientes = clientes::paginate($perPage);
        }
        foreach ($clientes as $clienteKey => $cliente)
        {
            $clientes[$clienteKey]->codigo = str_pad($cliente->id, 3, 0, STR_PAD_LEFT);
        }
        return view('clientes.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try
        {
            $requestData = $request->all();
            clientes::create($requestData);
            return redirect('clientes')->with('success_message', 'Cliente adicionado com sucesso!');
        }
        catch (QueryException $e)
        {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062)
            {
                return redirect()->back()->with('error_message', 'O cliente já existe!');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cliente = clientes::findOrFail($id);
        $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->where('movimentos.cliente_id', 'like', $id)
                                ->orderBy('created_at', 'desc')
                                ->paginate(5);

        $cliente->codigo = str_pad($cliente->id, 3, 0, STR_PAD_LEFT);
        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }
        return view('clientes.show', compact('cliente', 'movimentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cliente = clientes::findOrFail($id);

        return view('clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $cliente = clientes::findOrFail($id);
        $cliente->update($requestData);

        return redirect('clientes')->with('success_message', 'Cliente alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        clientes::destroy($id);

        return redirect('clientes')->with('success_message', 'Cliente deletado com sucesso!');
    }

}
