<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\movimentos;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use App\clientes;

class MovimentoClienteController extends Controller
{
    public function generatePDF(int $id)
    {
        $cliente        = clientes::findOrFail($id);
        $user           = Auth::user()->name;
        $myTime         = new \DateTime();
        if (!empty($cliente->nome))
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->where('movimentos.tipo', 'like', 'saida')
                                    ->where('clientes.id', '=', $id)
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->get();
            $count = count($movimentos);
            if ($count == 0)
            {
                return redirect()->back()->with('error_message', 'Não possivel emitir o relatório! O cliente não possui movimentação!');
            }
        }
        else
        {
           return redirect()->back()->with('error_message', 'Você precisa escolher pelo menos um cliente para emitir este relatório!');
        }

        $movimentacao = [];
        foreach ($movimentos as $movimento)
        {
            $movimento->codigo = str_pad($movimento->id, 4, 0, STR_PAD_LEFT);
            array_push($movimentacao, $movimento);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }
        $data =
        [
            'title'           => 'Relatório de Movimentações (Cliente)',
            'footer'          => 'DataFibra Estoque Fácil',
            'version'         => 'Versão 1.0.0',
            'current_user'    => $user,
            'movimentos'      => $movimentacao,
            'cliente'         => $cliente->nome,
            'footerDate'      => $myTime->format('d/m/Y H:i:s'),
            'img_path'        => '..\public\images\DataFibra.png'
        ];

        $pdf = PDF::loadView('relatorios/movimentoClienteRelatorio', $data);

        return $pdf->download('MovimentoCliente'.$cliente->nome.'.pdf');
    }
}
