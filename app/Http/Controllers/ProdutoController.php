<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\produtos;
use App\movimentos;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
use Illuminate\Database\QueryException;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $produtos = produtos::leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                                ->select('produtos.id', 'produtos.nome', 'produtos.marca', 'produtos.descricao', 'categoria.nome as categoria_nome')
                                ->where('id', 'like', '%'.$keyword.'%')
                                ->orWhere('nome', 'like', '%'.$keyword.'%')
                                ->orWhere('marca', 'like', '%'.$keyword.'%')
                                ->orWhere('categoria_id', 'like', '%'.$keyword.'%')
                                ->paginate($perPage);
        } else {
            $produtos = produtos::leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                                ->select('produtos.id', 'produtos.nome', 'produtos.marca', 'produtos.descricao', 'categorias.nome as categoria_nome')
                                ->paginate($perPage);
        }
        foreach ($produtos as $produtoKey => $produto)
        {
            $produtos[$produtoKey]->codigo = str_pad($produto->id, 3, 0, STR_PAD_LEFT);
        }
        return view('produtos.index', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('produtos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try
        {
            $requestData = $request->all();
            produtos::create($requestData);
            return redirect('produtos')->with('success_message', 'Produto adicionado com sucesso!');
        }
        catch (QueryException $e)
        {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062)
            {
                return redirect()->back()->with('error_message', 'O produto já existe!');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $produto = produtos::leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                            ->select('produtos.id', 'produtos.nome', 'produtos.marca', 'produtos.descricao', 'categorias.nome as categoria_nome')
                            ->findOrFail($id);

        $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                ->where('movimentos.produto_id', 'like', $id)
                                ->orderBy('created_at', 'asc')
                                ->orderBy('id', 'asc')
                                ->paginate(5);

        $produto->codigo = str_pad($produto->id, 3, 0, STR_PAD_LEFT);
        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }

        return view('produtos.show', compact('produto', 'movimentos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $produto = produtos::findOrFail($id);

        return view('produtos.edit', compact('produto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $produto = produtos::findOrFail($id);
        $produto->update($requestData);

        return redirect('produtos')->with('success_message', 'Produto alterado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        produtos::destroy($id);

        return redirect('produtos')->with('success_message', 'Produto deletado com sucesso!');
    }
}
