<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\movimentos;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;

class MovimentoPeriodoController extends Controller
{
    public function index(Request $request)
    {
        $dateBegin  = \DateTime::createfromFormat('d/m/Y H:i:s', $request->get('date1'));
        $dateEnd    = \DateTime::createfromFormat('d/m/Y H:i:s', $request->get('date2'));
        $keywordOne = '';
        $keywordTwo = '';
        if ($dateBegin instanceof \DateTime)
        {
            $keywordOne = $dateBegin->format('Y-m-d H:i:s');
        }
        if ($dateEnd instanceof \DateTime)
        {
            $keywordTwo = $dateEnd->format('Y-m-d H:i:s');
        }
        $perPage = 10;
        if (!empty($keywordOne && $keywordTwo))
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->whereBetween('movimentos.created_at', [$keywordOne, $keywordTwo])
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->paginate($perPage);
        }
        else
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->paginate($perPage);
        }

        foreach ($movimentos as $movimentKey => $movimento)
        {
            $movimentos[$movimentKey]->codigo = str_pad($movimento->id, 3, 0, STR_PAD_LEFT);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }

        return view('relatorios.movimentoPeriodo', compact('movimentos'));
    }

    public function generatePDF(Request $request)
    {
        $user = Auth::user()->name;
        $dateBegin  = \DateTime::createfromFormat('d/m/Y H:i:s', $request->get('date1'));
        $dateEnd    = \DateTime::createfromFormat('d/m/Y H:i:s', $request->get('date2'));
        $keywordOne = '';
        $keywordTwo = '';
        if ($dateBegin instanceof \DateTime)
        {
            $keywordOne = $dateBegin->format('Y-m-d H:i:s');
        }
        if ($dateEnd instanceof \DateTime)
        {
            $keywordTwo = $dateEnd->format('Y-m-d H:i:s');
        }
        // $keywordTwo = $request->get('date2');
        //("d/m/Y h:i:s", time());
        $myTime = new \DateTime();
        //dd($keywordOne, $keywordTwo);

        if (!empty($keywordOne && $keywordTwo))
        {
            $movimentos = movimentos::leftjoin('fornecedores', 'movimentos.fornecedor_id', 'like', 'fornecedores.id')
                                    ->leftjoin('clientes', 'movimentos.cliente_id', 'like', 'clientes.id')
                                    ->leftjoin('produtos', 'movimentos.produto_id', 'like', 'produtos.id')
                                    ->select('movimentos.id', 'movimentos.created_at', 'fornecedores.nome as fornecedor_nome', 'clientes.nome as cliente_nome', 'produtos.nome as produto_nome', 'movimentos.tipo', 'movimentos.motivo', 'movimentos.quantidade', 'movimentos.valor_unit', 'movimentos.valor_total')
                                    ->whereBetween('movimentos.created_at', [$keywordOne, $keywordTwo])
                                    ->orderBy('created_at', 'asc')
                                    ->orderBy('id', 'asc')
                                    ->get();
        }
        else
        {
           return redirect('relatorios/movimentoperiodo')->with('error_message', 'Você precisa definir um periodo para emitir este relatório!');
        }
        $movimentacao = [];
        foreach ($movimentos as $movimento)
        {
            $movimento->codigo = str_pad($movimento->id, 4, 0, STR_PAD_LEFT);
            array_push($movimentacao, $movimento);
            if ($movimento->fornecedor_nome == '')
            {
                $movimento->fornecedor_nome = '-';
            }
            else
            {
                $movimento->cliente_nome = '-';
            }
        }
        $data =
        [
            'title'           => 'Relatório de Movimentações (Periodo)',
            'footer'          => 'DataFibra Estoque Fácil',
            'version'         => 'Versão 1.0.0',
            'current_user'    => $user,
            'movimentos'      => $movimentacao,
            'footerDate'      => $myTime->format('d/m/Y H:i:s'),
            'periodoInicio'   => $dateBegin->format('d/m/Y H:i:s'),
            'periodoFim'      => $dateEnd->format('d/m/Y H:i:s'),
            'img_path'        => '..\public\images\DataFibra.png'
        ];

        $pdf = PDF::loadView('relatorios/movimentoPeriodoRelatorio', $data);

        return $pdf->download('Movimento_'.$keywordOne.'-'.$keywordTwo.'.pdf');
    }
}
