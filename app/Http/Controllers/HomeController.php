<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produtos;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;
        if (!empty($keyword)) {
            $produtos = produtos::leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                                ->select('produtos.id', 'produtos.nome', 'produtos.marca', 'produtos.descricao', 'categorias.nome as categoria_nome', 'produtos.quantidade', 'produtos.custo_medio', 'produtos.valor_total')
                                ->where('produtos.id', 'like', '%'.$keyword.'%')
                                ->orWhere('produtos.nome', 'like', '%'.$keyword.'%')
                                ->orWhere('produtos.marca', 'like', '%'.$keyword.'%')
                                ->orWhere('categorias.nome', 'like', '%'.$keyword.'%')
                                ->orWhere('produtos.quantidade', 'like', '%'.$keyword.'%')
                                ->orWhere('produtos.custo_medio', 'like', '%'.$keyword.'%')
                                ->orWhere('produtos.valor_total', 'like', '%'.$keyword.'%')
                                ->paginate($perPage);
        } else {
            $produtos = produtos::leftjoin('categorias', 'produtos.categoria_id', 'like', 'categorias.id')
                                ->select('produtos.id', 'produtos.nome', 'produtos.marca', 'produtos.descricao', 'categorias.nome as categoria_nome', 'produtos.quantidade', 'produtos.custo_medio', 'produtos.valor_total')
                                ->paginate($perPage);
        }
        foreach ($produtos as $produtoKey => $produto)
        {
            $produtos[$produtoKey]->codigo = str_pad($produto->id, 3, 0, STR_PAD_LEFT);
        }
        return view('home', compact('produtos'));
    }
}
